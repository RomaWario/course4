let start_table = [];
let alphabet1 = ["a", "b", "c", "d", "e", "f", "g", "h"];
let alphabet = alphabet1.reverse();
let numbers1 = [8, 7, 6, 5, 4, 3, 2, 1];
let numbers = numbers1.reverse();
let selectedTd = 0;
let flag = false;
let possible_moves = [];
let obligatory_moves = [];
let checkers_on_obligatory_moves = [];
let knocked_checker_by_obligatory_moves = new Map();
let count_moves = 0;
const WHITE_PLAYER = "white-checker", BLACK_PLAYER = "black-checker";
let player_move = WHITE_PLAYER;
const
    BLACK_CHECKER = 2,
    WHITE_CHECKER = 1,
    BLACK_CROWN_CHECKER = 4,
    WHITE_CROWN_CHECKER = 3,
    EMPTY_CELL = 0;
let state_buttons_info = false;
let last_move_from, last_move_to;

function start_init_table() {
    let type_checker = WHITE_CHECKER;
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(i > 2){
                type_checker = BLACK_CHECKER;
            }
            if (i === 3 || i === 4) {
                start_table[i][j] = EMPTY_CELL;
            } else {
                if (i % 2 === 0) {
                    if (j % 2 === 0) {
                        start_table[i][j] = EMPTY_CELL;
                    } else {
                        start_table[i][j] = type_checker;
                    }
                } else {
                    if (j % 2 === 0) {
                        start_table[i][j] = type_checker;
                    } else {
                        start_table[i][j] = EMPTY_CELL;
                    }
                }
            }
        }
    }
}

function getIndexByChar(char){
    return alphabet.indexOf(char);
}

function getIndexByNumber(num){
    return numbers.indexOf(num);
}

function example_init_table(){
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(j === getIndexByChar('f') && i === getIndexByNumber(4) ||
                j === getIndexByChar('h') && i === getIndexByNumber(4)){
                start_table[i][j] = WHITE_CHECKER;
            }else if(
                j === getIndexByChar('b') && i === getIndexByNumber(8) ||
                j === getIndexByChar('c') && i === getIndexByNumber(5) ||
                j === getIndexByChar('c') && i === getIndexByNumber(7) ||
                j === getIndexByChar('e') && i === getIndexByNumber(7) ||
                j === getIndexByChar('h') && i === getIndexByNumber(6)){
                start_table[i][j] = BLACK_CHECKER;
            }else if(j === getIndexByChar('c') && i === getIndexByNumber(1)) {
                start_table[i][j] = BLACK_CROWN_CHECKER;
            }else {
                start_table[i][j] = EMPTY_CELL;
            }
        }
    }
}

function clear(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].hasChildNodes()){
            let img = blacks[i].firstChild.remove();
        }
        if(blacks[i].style.backgroundColor === 'yellow'){
            blacks[i].style.backgroundColor = '#a9a9a9';
        }
    }
    reset_color_moves();
}

function create_img(type, nameClass){
    let img = document.createElement("img");
    img.setAttribute("src", "img/" + type + ".png");
    img.setAttribute("height", "31px");
    img.setAttribute("width", "30px");
    img.setAttribute("alt", type);
    img.setAttribute("class", nameClass);
    return img;
}

function print_table(table){
    clear();
    for (let i = 0; i < 8; i++) {
        let number = numbers[i];
        for (let j = 0; j < 8; j++) {
            let elem = document.getElementById(alphabet[j] + number);
            if(table[i][j] === WHITE_CHECKER){
                elem.appendChild(create_img("white", "white-checker"));
            }

            if(table[i][j] === BLACK_CHECKER){
                elem.appendChild(create_img("black", "black-checker"));
            }

            if(table[i][j] === WHITE_CROWN_CHECKER){
                elem.appendChild(create_img("white_crown", "white-checker"));
            }

            if(table[i][j] === BLACK_CROWN_CHECKER){
                elem.appendChild(create_img("black_crown", "black-checker"));
            }
        }
    }
}

let start_btn = document.querySelector(".start-button");
start_btn.addEventListener("click", function (){
    state_buttons_info = false;
    setActiveButtonsInfoMove(state_buttons_info);
    start_init_table();
    print_table(start_table);
    changeInfoPlayerMove(WHITE_PLAYER);
    remove_recording_moves();
});

let exp_btn = document.querySelector(".example-button");
exp_btn.addEventListener("click", function (){
    state_buttons_info = false;
    setActiveButtonsInfoMove(state_buttons_info);
    example_init_table();
    print_table(start_table);
    changeInfoPlayerMove(WHITE_PLAYER);
    remove_recording_moves();
});

let cur_state = document.querySelector(".current-state-table-button");
cur_state.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    popup.style.display = 'block';
    popup.style.position = 'absolute';
    let text = current_state_table();
    let wh = text.slice(0, text.indexOf("Черные"));
    let bl = text.slice(text.indexOf("Черные"));
    let p = document.createElement("p");
    p.className = "text-black";
    p.textContent = wh;
    let p2 = document.createElement("p");
    p2.className = "text-white";
    p2.textContent = bl;
    popup.appendChild(p);
    popup.appendChild(p2);
});

let close_btn = document.querySelector(".popup-close-button");
close_btn.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    let p1 = document.querySelector(".text-black");
    let p2 = document.querySelector(".text-white");
    p1.remove();
    p2.remove();
    popup.style.display = 'none';
});

let table = document.querySelector("table");

function checkActive(td){
    if(td.id === selectedTd.id){
        return true;
    }
    return selectedTd.style.backgroundColor !== 'yellow';
}

function checkerToCrown(td, typeChecker){
    if(typeChecker !== WHITE_CROWN_CHECKER && typeChecker !== BLACK_CROWN_CHECKER){
        return;
    }
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    start_table[i][j] = typeChecker;
}

function show_moves_black(td){
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 0){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num - 1;
    let j = index_char + 1;
    //Ход верх-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
    }
    if(!h && start_table[i][j] === WHITE_CHECKER || !h && start_table[i][j] === WHITE_CROWN_CHECKER){
        let k = i - 1;
        let l = j + 1;
        if(l <= 7 && k >= 0){
            remove_green_moves();
            if(start_table[k][l] === EMPTY_CELL){
                let td = document.getElementById(alphabet[l] + numbers[k]);
                let brokCheckerTd = getTdByCoordinate(i, j);
                knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                // console.log(td);
                // console.log(brokCheckerTd);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
    //Ход верх-лево
    i = index_num - 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
        return;
    }
    if(!a && start_table[i][j] === WHITE_CHECKER || !a && start_table[i][j] === WHITE_CROWN_CHECKER) {
        let k = i - 1;
        let l = j - 1;
        if (l >= 0 && k >= 0) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                let brokCheckerTd = getTdByCoordinate(i, j);
                knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                // console.log(td);
                // console.log(brokCheckerTd);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_white(td){
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 7){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num + 1;
    let j = index_char + 1;
    //Ход низ-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
    }
    if(!h && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
        let k = i + 1;
        let l = j + 1;
        if(l <= 7 && k <= 7){
            if(start_table[k][l] === EMPTY_CELL){
                let td = document.getElementById(alphabet[l] + numbers[k]);
                let brokCheckerTd = getTdByCoordinate(i, j);
                knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                // console.log(td);
                // console.log(brokCheckerTd);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
    //Ход низ-лево
    i = index_num + 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
        return;
    }
    if(!a && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)) {
        let k = i + 1;
        let l = j - 1;
        if (l >= 0 && k <= 7) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                let brokCheckerTd = getTdByCoordinate(i, j);
                knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                // console.log(td);
                // console.log(brokCheckerTd);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_crown(td){
    let type  = getTypeCheckerByTd(td);
    let coordI = getCoordinateIByTd(td);
    let coordJ = getCoordinateJByTd(td);
    let blockBR = false, blockBL = false, blockTR = false, blockTL = false;
    let bottomRight = 1, bottomLeft = 1, topRight = 1, topLeft = 1;
    for (let i = coordI; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI + bottomRight && j === coordJ + bottomRight){
                //BR
                if(!blockBR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }

                if(!blockBR && i < 7 && j < 7 && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i + 1, l = j + 1;
                    // console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        // let td = document.getElementById(alphabet[l] + numbers[k]);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }

                if(!blockBR && i < 7 && j < 7 &&(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i + 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }
                bottomRight++;
            }
            if(i === coordI + bottomLeft && j === coordJ - bottomLeft){
                //BL
                if(!blockBL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }
                if(!blockBL && i < 7 && j > 0 &&(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }

                if(!blockBL && i < 7 && j > 0 &&(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }
                bottomLeft++;
            }
        }
    }
    for (let i = coordI; i >= 0; i--) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI - topRight && j === coordJ + topRight){
                //TR
                if(!blockTR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }

                if(!blockTR && i > 0 && j < 7 && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i - 1, l = j + 1;
                    // console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }

                if(!blockTR && i > 0 && j < 7 && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i - 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }
                topRight++;
            }
            if(i === coordI - topLeft && j === coordJ - topLeft){
                //TL
                if(!blockTL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }
                if(!blockTL && i > 0 && j > 0 &&(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }

                if(!blockTL && i > 0 && j > 0 && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }
                topLeft++;
            }
        }
    }
}

function reset_color_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'||
            blacks[i].style.border === '3px solid red'){
            blacks[i].style.border = '1px solid black';
        }
    }
    possible_moves = [];
    obligatory_moves = [];
    // knocked_checker_by_obligatory_moves = new Map();
}

function remove_green_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'){
            blacks[i].style.border = '1px solid black';
        }
    }
     possible_moves = [];
}

function make_move(from, to){
    last_move_from = from;
    last_move_to = to;
    let i = getCoordinateIByTd(from);
    let j = getCoordinateJByTd(from);
    let k = getCoordinateIByTd(to);
    let l = getCoordinateJByTd(to);
    let type = getTypeCheckerByTd(from);
    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    start_table[k][l] = temp;
    print_table(start_table);
    state_buttons_info = true;
    setActiveButtonsInfoMove(state_buttons_info);
}

function undo_last_move(){
    let i = getCoordinateIByTd(last_move_from);
    let j = getCoordinateJByTd(last_move_from);
    let k = getCoordinateIByTd(last_move_to);
    let l = getCoordinateJByTd(last_move_to);
    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    start_table[k][l] = temp;
    print_table(start_table);
}

function changeInfoPlayerMove(player){
    let info = document.querySelector(".player-move-info");
    if(player === WHITE_PLAYER){
        info.textContent = "Белый";
    }else{
        info.textContent = "Черный";
    }
    player_move = player;
}

function getTypeCheckerByTd(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    return start_table[i][j];
}

function getTdByCoordinate(i, j){
   return document.getElementById(alphabet[j] + numbers[i]);
}

function getCoordinateIByTd(td){
    // console.log(td);
    return numbers.indexOf(Number(td.id.toString().slice(1)));;
}

function getCoordinateJByTd(td){
    return alphabet.indexOf(td.id.toString().slice(0, 1));
}

function highlight_cell(td){
    if(td.style.backgroundColor === 'yellow'){
        reset_color_moves();
        selectedTd.style.backgroundColor = '#a9a9a9';
    }else {
        selectedTd = td;
        selectedTd.style.backgroundColor = 'yellow';
        flag = true;
    }
}

function current_state_table(){
    let white = 'Белые: ';
    let black = 'Черные: ';
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            let td = getTdByCoordinate(i, j);
            if(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
                white += td.id.toString() + ' ';
            }
            if(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER){
                black += td.id.toString() + ' ';
            }
        }
    }
    return white + black;
}

start_init_table();
print_table(start_table);

function handler_moves(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    if(start_table[i][j] === BLACK_CROWN_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
        show_moves_crown(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === BLACK_CHECKER){
        show_moves_black(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === WHITE_CHECKER){
        show_moves_white(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
}

table.onclick = function (event){
    let td = event.target.closest('td'); // (1)
  //  if (td.className !== "black") return; // (2)
    if (!table.contains(td)) return;
    if(state_buttons_info){
        return;
    }
    checkers_on_obligatory_moves = [];
    view_checkers_on_obligatory_moves(td);
    if(checkers_on_obligatory_moves.length !== 0 && !checkers_on_obligatory_moves.includes(td)){
        return;
    }
    if(obligatory_moves.length === 0){
        for (let i = 0; i < possible_moves.length; i++) {
            if(td === possible_moves[i]){
                make_move(selectedTd, possible_moves[i]);
                return ;
            }
        }
    }else {
        for (let i = 0; i < obligatory_moves.length; i++) {
            if (td === obligatory_moves[i]) {
                make_move(selectedTd, obligatory_moves[i]);
                return;
            }
        }
    }
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    if(start_table[i][j] === EMPTY_CELL || !checkPlayerMove(td)){
        return;
    }
    if(flag && !checkActive(td))  return;
    if(obligatory_moves.size === 0){
        return ;
    }
    // if(checkers_on_obligatory_moves.length === 0){
    //     handler_moves(td);
    // }
    handler_moves(td);
    highlight_cell(td);
}

function view_checkers_on_obligatory_moves(td){
    let type = getTypeCheckerByTd(td);
    let white = false;
    let black = false;
    if(type === WHITE_CHECKER || type === WHITE_CROWN_CHECKER ){
        white = true;
    }else if(type === BLACK_CHECKER || type === BLACK_CROWN_CHECKER ){
        black = true;
    }
    if(white && player_move === WHITE_PLAYER ||
        black && player_move === BLACK_PLAYER) {
        console.log("***********")
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if(white && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let td = getTdByCoordinate(i, j);
                    handler_moves(td);
                    if (obligatory_moves.length !== 0) {
                        checkers_on_obligatory_moves.push(td);
                    }
                    reset_color_moves();
                }

                if(black && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let td = getTdByCoordinate(i, j);
                    handler_moves(td);
                    if (obligatory_moves.length !== 0) {
                        checkers_on_obligatory_moves.push(td);
                    }
                    reset_color_moves();
                }

            }
        }
    }
}

function checkPlayerMove(td){
    if(!td.hasChildNodes()){
        return false;
    }
    return td.firstChild.className === player_move;
}

function setActiveButtonsInfoMove(active){
    let button1 = document.querySelector(".complete-move-btn");
    let button2 = document.querySelector(".undo-move-btn");
    if(active){
        button1.style.backgroundColor = "#007765";
        button2.style.backgroundColor = "#007765";
    }else{
        button1.style.backgroundColor = "darkgrey";
        button2.style.backgroundColor = "darkgrey";
    }
}

function make_recording_moves(){

    let recordGame = document.querySelector(".recording-game-show");
    let div = document.createElement("div");
    if(player_move === WHITE_PLAYER){
        div.setAttribute("class", "recording-moves-left");
    }else{
        count_moves++;
        div.setAttribute("class", "recording-moves-right");
    }
   // div.setAttribute("class", "recording-moves");
    let symbol = '-';
    if(knocked_checker_by_obligatory_moves.size !== 0){
        symbol = ':';
    }
    if(player_move === BLACK_PLAYER){
        div.textContent = count_moves.toString() + "." + last_move_from.id.toString() + symbol + last_move_to.id.toString();
    }else{
        div.textContent = last_move_from.id.toString() + symbol + last_move_to.id.toString();

    }
    recordGame.appendChild(div);
}

function remove_recording_moves(){
    let rcrd = document.querySelector(".recording-game-show");
    let moves = document.getElementsByClassName("recording-moves");
    let size = moves.length;
    for (let i = size - 1; i >= 0; i--) {
         rcrd.removeChild(moves[i]);
    }
    count_moves = 0;
}

let complete_btn = document.querySelector(".complete-move-btn");
complete_btn.addEventListener("click", function (){
    if(state_buttons_info){
        state_buttons_info = false;
        setActiveButtonsInfoMove(state_buttons_info);
        if(player_move === WHITE_PLAYER){
            player_move = BLACK_PLAYER;
        }else{
            player_move = WHITE_PLAYER;
        }
        let i = getCoordinateIByTd(last_move_to);
        let type = getTypeCheckerByTd(last_move_to);
        if(i === 0 && type === BLACK_CHECKER){
            checkerToCrown(last_move_to, BLACK_CROWN_CHECKER);
        }
        if(i === 7 && type === WHITE_CHECKER){
            checkerToCrown(last_move_to, WHITE_CROWN_CHECKER);
        }

        if(knocked_checker_by_obligatory_moves.size !== 0){
            knocked_checker(last_move_to);
        }
        print_table(start_table);
        changeInfoPlayerMove(player_move);
        make_recording_moves();
        knocked_checker_by_obligatory_moves.clear();
    }
});
let undo_btn = document.querySelector(".undo-move-btn");
undo_btn.addEventListener("click", function (){
    if(state_buttons_info){
        undo_last_move();
        state_buttons_info = false;
        setActiveButtonsInfoMove(state_buttons_info);
        knocked_checker_by_obligatory_moves.clear();
         // reset_color_moves();
    }
});

function knocked_checker(checker){ //срубить шашку
    let td = knocked_checker_by_obligatory_moves.get(checker);
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    start_table[i][j] = 0;
    print_table(start_table);
}

// let read_btn = document.querySelector(".read-record-button");
// read_btn.addEventListener("click", function (){
//     readTextArea();
// });

function getTdById(id){
    return document.getElementById(id);
}

function readTextArea(){
    let area = document.querySelector(".read-record-textarea");
    let text = area.value;
    let isColon = false;
    if(text !== ""){
        let arrOfStrings = text.toString().split("\n");
        for (let index = 0; index < arrOfStrings.length; index++) {
            let posPoint, posDash, posColonOrDash;
            if((posPoint = arrOfStrings[index].indexOf(".")) === -1){
                alert("Ошибка в записи " + arrOfStrings[index]);
            }
            if((posColonOrDash = arrOfStrings[index].indexOf("-")) === -1){
                if((posColonOrDash = arrOfStrings[index].indexOf(":")) === -1){
                    alert("Ошибка в записи " + arrOfStrings[index]);
                }
                isColon = true;
            }
            let idFrom = arrOfStrings[index].toString().substring(posPoint + 1, posPoint + 3);
            let idTo = arrOfStrings[index].substring(posColonOrDash + 1, posColonOrDash + 3);
            let i = getCoordinateIByTd(getTdById(idFrom));
            let j = getCoordinateJByTd(getTdById(idFrom));
            let k = getCoordinateIByTd(getTdById(idTo));
            let l = getCoordinateJByTd(getTdById(idTo));

            if(start_table[i][j] !== EMPTY_CELL && start_table[k][l] === EMPTY_CELL){
                // console.log(posColonOrDash);
                if(isColon){
                    let id = getIdBetweenFromAndTo(idFrom, idTo);
                    let index_num_id = getIndexByNumber(Number(id.slice(1)));
                    let index_char_id = getIndexByChar(id.slice(0, 1));
                    // console.log(getTdByCoordinate(index_num_id, index_char_id));
                    start_table[index_num_id][index_char_id] = 0;
                }
                let temp = start_table[i][j];
                start_table[i][j] = start_table[k][l];
                start_table[k][l] = temp;
                print_table(start_table);
            }
        }
    }else{
        alert("ПУСТО");
    }
}

function getIdBetweenFromAndTo(from, to){
    let result_index_num, result_index_char;
    let char_from = from.slice(0, 1);
    let num_from = from.slice(1);
    let char_to = to.slice(0, 1);
    let num_to = to.slice(1);
    let index_char_from = getIndexByChar(char_from);
    let index_num_from = getIndexByNumber(Number(num_from));
    let index_char_to = getIndexByChar(char_to);
    let index_num_to = getIndexByNumber(Number(num_to));

    if(index_num_from > index_num_to){
        result_index_num = index_num_to + 1;
    }else{
        result_index_num = index_num_to - 1;
    }
    if(index_char_from > index_char_to){
        result_index_char = index_char_to + 1;
    }else{
        result_index_char = index_char_to - 1;
    }
    return alphabet[result_index_char] + numbers[result_index_num];
}

