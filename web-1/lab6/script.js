let start_table = [];
let alphabet1 = ["a", "b", "c", "d", "e", "f", "g", "h"];
let alphabet = alphabet1.reverse();
let numbers1 = [8, 7, 6, 5, 4, 3, 2, 1];
let numbers = numbers1.reverse();
let selectedTd = 0;
let flag = false;
let possible_moves = [];
let obligatory_moves = [];
let checkers_on_obligatory_moves = [];
let knocked_checker_by_obligatory_moves = new Map();
let count_moves = 0;
const WHITE_PLAYER = "white-checker", BLACK_PLAYER = "black-checker";
let WIN;
let player_move = WHITE_PLAYER;
const
    BLACK_CHECKER = 2,
    WHITE_CHECKER = 1,
    BLACK_CROWN_CHECKER = 4,
    WHITE_CROWN_CHECKER = 3,
    EMPTY_CELL = 0;
let state_buttons_info = false;
let last_move_from, last_move_to;

function start_init_table() {
    let type_checker = WHITE_CHECKER;
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(i > 2){
                type_checker = BLACK_CHECKER;
            }
            if (i === 3 || i === 4) {
                start_table[i][j] = EMPTY_CELL;
            } else {
                if (i % 2 === 0) {
                    if (j % 2 === 0) {
                        start_table[i][j] = EMPTY_CELL;
                    } else {
                        start_table[i][j] = type_checker;
                    }
                } else {
                    if (j % 2 === 0) {
                        start_table[i][j] = type_checker;
                    } else {
                        start_table[i][j] = EMPTY_CELL;
                    }
                }
            }
        }
    }
}

function getIndexByChar(char){
    return alphabet.indexOf(char);
}

function getIndexByNumber(num){
    return numbers.indexOf(num);
}

function example_init_table(){
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(j === getIndexByChar('f') && i === getIndexByNumber(4) ||
                j === getIndexByChar('h') && i === getIndexByNumber(4)){
                start_table[i][j] = WHITE_CHECKER;
            }else if(
                j === getIndexByChar('b') && i === getIndexByNumber(8) ||
                j === getIndexByChar('c') && i === getIndexByNumber(5) ||
                j === getIndexByChar('c') && i === getIndexByNumber(7) ||
                j === getIndexByChar('e') && i === getIndexByNumber(7) ||
                j === getIndexByChar('h') && i === getIndexByNumber(6)){
                start_table[i][j] = BLACK_CHECKER;
            }else if(j === getIndexByChar('c') && i === getIndexByNumber(1)) {
                start_table[i][j] = BLACK_CROWN_CHECKER;
            }else {
                start_table[i][j] = EMPTY_CELL;
            }
        }
    }
}

function clear(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].hasChildNodes()){
            let img = blacks[i].firstChild.remove();
        }
        if(blacks[i].style.backgroundColor === 'yellow'){
            blacks[i].style.backgroundColor = '#a9a9a9';
        }
    }
    reset_color_moves();
}

function create_img(type, nameClass){
    let img = document.createElement("img");
    img.setAttribute("src", "img/" + type + ".png");
    img.setAttribute("height", "31px");
    img.setAttribute("width", "30px");
    img.setAttribute("alt", type);
    img.setAttribute("class", nameClass);
    return img;
}

function print_table(table){
    clear();
    for (let i = 0; i < 8; i++) {
        let number = numbers[i];
        for (let j = 0; j < 8; j++) {
            let elem = document.getElementById(alphabet[j] + number);
            if(table[i][j] === WHITE_CHECKER){
                elem.appendChild(create_img("white", "white-checker"));
            }

            if(table[i][j] === BLACK_CHECKER){
                elem.appendChild(create_img("black", "black-checker"));
            }

            if(table[i][j] === WHITE_CROWN_CHECKER){
                elem.appendChild(create_img("white_crown", "white-checker"));
            }

            if(table[i][j] === BLACK_CROWN_CHECKER){
                elem.appendChild(create_img("black_crown", "black-checker"));
            }
        }
    }
}

let start_btn = document.querySelector(".start-button");
start_btn.addEventListener("click", function (){
    state_buttons_info = false;
    setActiveButtonsInfoMove(state_buttons_info);
    start_init_table();
    print_table(start_table);
    changeInfoPlayerMove(WHITE_PLAYER);
    remove_recording_moves();
});

let exp_btn = document.querySelector(".example-button");
exp_btn.addEventListener("click", function (){
    state_buttons_info = false;
    setActiveButtonsInfoMove(state_buttons_info);
    example_init_table();
    print_table(start_table);
    changeInfoPlayerMove(WHITE_PLAYER);
    remove_recording_moves();
});

let cur_state = document.querySelector(".current-state-table-button");
cur_state.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    popup.style.display = 'block';
    popup.style.position = 'absolute';
    let text = current_state_table();
    let wh = text.slice(0, text.indexOf("Черные"));
    let bl = text.slice(text.indexOf("Черные"));
    let p = document.createElement("p");
    p.className = "text-black";
    p.textContent = wh;
    let p2 = document.createElement("p");
    p2.className = "text-white";
    p2.textContent = bl;
    popup.appendChild(p);
    popup.appendChild(p2);
});

let close_btn = document.querySelector(".popup-close-button");
close_btn.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    let childs = popup.childNodes;
    for (let i = 0; i < childs.length; i++) {
        console.log(childs[i].nodeName);
        // if(childs[i].nodeName === "P"){
        //     // childs[i].remove();
        //     popup.removeChild(childs[i]);
        // }
        if(popup.lastChild.nodeName !== "P"){
            continue;
        }
        popup.lastChild.remove();
    }

    // while(popup.hasChildNodes()){
    //
    // }
    // let p1 = document.querySelector(".text-black");
    // let p2 = document.querySelector(".text-white");
    // p1.remove();
    // p2.remove();
    popup.style.display = 'none';
});

let table = document.querySelector("table");

function checkActive(td){
    if(td.id === selectedTd.id){
        return true;
    }
    return selectedTd.style.backgroundColor !== 'yellow';
}

function checkerToCrown(td, typeChecker){
    if(typeChecker !== WHITE_CROWN_CHECKER && typeChecker !== BLACK_CROWN_CHECKER){
        return;
    }
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    start_table[i][j] = typeChecker;
}

function show_moves_black(td, flag){
    //если флаг = false, то стили не применяются к клеткам
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 0){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num - 1;
    let j = index_char + 1;
    //Ход верх-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        if(flag){
            td.style.border = '3px solid green';
        }
    }
    if(!h && start_table[i][j] === WHITE_CHECKER || !h && start_table[i][j] === WHITE_CROWN_CHECKER){
        let k = i - 1;
        let l = j + 1;
        if(l <= 7 && k >= 0){
            remove_green_moves();
            if(start_table[k][l] === EMPTY_CELL){
                 let td = document.getElementById(alphabet[l] + numbers[k]);
                 if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                       
                        td.style.border = '3px solid red';
                }

                obligatory_moves.push(td);
            }
        }
    }
    //Ход верх-лево
    i = index_num - 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        if(flag){
            td.style.border = '3px solid green';
        }

        return;
    }
    if(!a && start_table[i][j] === WHITE_CHECKER || !a && start_table[i][j] === WHITE_CROWN_CHECKER) {
        let k = i - 1;
        let l = j - 1;
        if (l >= 0 && k >= 0) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                 if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        // console.log(td);
                        // console.log(brokCheckerTd);

                            td.style.border = '3px solid red';
                }
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_white(td, flag){
    //flag = false, то стили не применяются
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 7){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num + 1;
    let j = index_char + 1;
    //Ход низ-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        if(flag){
            td.style.border = '3px solid green';
        }
    }
    if(!h && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
        let k = i + 1;
        let l = j + 1;
        if(l <= 7 && k <= 7){
            if(start_table[k][l] === EMPTY_CELL){
                let td = document.getElementById(alphabet[l] + numbers[k]);
                 if(flag){
                         let brokCheckerTd = getTdByCoordinate(i, j);
                         knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                // console.log(td);
                // console.log(brokCheckerTd);

                          td.style.border = '3px solid red';
                }
                obligatory_moves.push(td);
            }
        }
    }
    //Ход низ-лево
    i = index_num + 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        if(flag){
            td.style.border = '3px solid green';
        }
        return;
    }
    if(!a && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)) {
        let k = i + 1;
        let l = j - 1;
        if (l >= 0 && k <= 7) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                 if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);
                        // console.log(td);
                        // console.log(brokCheckerTd);

                            td.style.border = '3px solid red';
                }
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_crown(td, flag){
    let type  = getTypeCheckerByTd(td);
    let coordI = getCoordinateIByTd(td);
    let coordJ = getCoordinateJByTd(td);
    let blockBR = false, blockBL = false, blockTR = false, blockTL = false;
    let bottomRight = 1, bottomLeft = 1, topRight = 1, topLeft = 1;
    for (let i = coordI; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI + bottomRight && j === coordJ + bottomRight){
                //BR
                if(!blockBR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    if(flag){
                        td.style.border = '3px solid green';
                    }
                    possible_moves.push(td);
                }

                if(!blockBR && i < 7 && j < 7 && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i + 1, l = j + 1;
                    // console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        // let td = document.getElementById(alphabet[l] + numbers[k]);
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }

                if(!blockBR && i < 7 && j < 7 &&(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i + 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){  
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }
                bottomRight++;
            }
            if(i === coordI + bottomLeft && j === coordJ - bottomLeft){
                //BL
                if(!blockBL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    if(flag){
                        td.style.border = '3px solid green';
                    }
                    possible_moves.push(td);
                }
                if(!blockBL && i < 7 && j > 0 &&(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }

                if(!blockBL && i < 7 && j > 0 &&(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }
                bottomLeft++;
            }
        }
    }
    for (let i = coordI; i >= 0; i--) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI - topRight && j === coordJ + topRight){
                //TR
                if(!blockTR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    if(flag){
                        td.style.border = '3px solid green';
                    }
                    possible_moves.push(td);
                }

                if(!blockTR && i > 0 && j < 7 && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i - 1, l = j + 1;
                    // console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }

                if(!blockTR && i > 0 && j < 7 && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i - 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }
                topRight++;
            }
            if(i === coordI - topLeft && j === coordJ - topLeft){
                //TL
                if(!blockTL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    if(flag){
                        td.style.border = '3px solid green';
                    }
                    possible_moves.push(td);
                }
                if(!blockTL && i > 0 && j > 0 &&(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }

                if(!blockTL && i > 0 && j > 0 && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        if(flag){
                        let brokCheckerTd = getTdByCoordinate(i, j);
                        knocked_checker_by_obligatory_moves.set(td, brokCheckerTd);

                            td.style.border = '3px solid red';
                        }
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }
                topLeft++;
            }
        }
    }
}

function reset_color_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'||
            blacks[i].style.border === '3px solid red'){
            blacks[i].style.border = '1px solid black';
        }
    }
    possible_moves = [];
    obligatory_moves = [];
    // knocked_checker_by_obligatory_moves = new Map();
}

function remove_green_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'){
            blacks[i].style.border = '1px solid black';
        }
    }
     possible_moves = [];
}

function make_move(from, to){
    last_move_from = from;
    last_move_to = to;
    let i = getCoordinateIByTd(from);
    let j = getCoordinateJByTd(from);
    let k = getCoordinateIByTd(to);
    let l = getCoordinateJByTd(to);
    let type = getTypeCheckerByTd(from);
    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    start_table[k][l] = temp;
    print_table(start_table);
    state_buttons_info = true;
    setActiveButtonsInfoMove(state_buttons_info);
}

function undo_last_move(){
    let i = getCoordinateIByTd(last_move_from);
    let j = getCoordinateJByTd(last_move_from);
    let k = getCoordinateIByTd(last_move_to);
    let l = getCoordinateJByTd(last_move_to);
    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    start_table[k][l] = temp;
    print_table(start_table);
}

function changeInfoPlayerMove(player){
    let info = document.querySelector(".player-move-info");
    if(player === WHITE_PLAYER){
        info.textContent = "Белый";
    }else{
        info.textContent = "Черный";
    }
    player_move = player;
}

function getTypeCheckerByTd(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    return start_table[i][j];
}

function getTdByCoordinate(i, j){
   return document.getElementById(alphabet[j] + numbers[i]);
}

function getCoordinateIByTd(td){
    // console.log(td);
    return numbers.indexOf(Number(td.id.toString().slice(1)));;
}

function getCoordinateJByTd(td){
    return alphabet.indexOf(td.id.toString().slice(0, 1));
}

function highlight_cell(td){
    if(td.style.backgroundColor === 'yellow'){
        reset_color_moves();
        selectedTd.style.backgroundColor = '#a9a9a9';
    }else {
        selectedTd = td;
        selectedTd.style.backgroundColor = 'yellow';
        flag = true;
    }
}

function current_state_table(){
    let white = 'Белые: ';
    let black = 'Черные: ';
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            let td = getTdByCoordinate(i, j);
            if(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
                white += td.id.toString() + ' ';
            }
            if(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER){
                black += td.id.toString() + ' ';
            }
        }
    }
    return white + black;
}

start_init_table();
print_table(start_table);

function handler_moves(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    if(start_table[i][j] === BLACK_CROWN_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
        show_moves_crown(td, true);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === BLACK_CHECKER){
        show_moves_black(td, true);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === WHITE_CHECKER){
        show_moves_white(td, true);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
}

table.onclick = function (event){
    let td = event.target.closest('td'); // (1)
  //  if (td.className !== "black") return; // (2)
    if (!table.contains(td)) return;

    if(state_buttons_info){
        return;
    }

    checkers_on_obligatory_moves = [];
    view_checkers_on_obligatory_moves(td);
    if(checkers_on_obligatory_moves.length !== 0 && !checkers_on_obligatory_moves.includes(td)){
        return;
    }
    if(obligatory_moves.length === 0){
        for (let i = 0; i < possible_moves.length; i++) {
            if(td === possible_moves[i]){
                make_move(selectedTd, possible_moves[i]);
                return ;
            }
        }
    }else {
        for (let i = 0; i < obligatory_moves.length; i++) {
            if (td === obligatory_moves[i]) {
                make_move(selectedTd, obligatory_moves[i]);
                return;
            }
        }
    }
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    if(start_table[i][j] === EMPTY_CELL || !checkPlayerMove(td)){
        return;
    }

    if(flag && !checkActive(td))  return;
    handler_moves(td);
    highlight_cell(td);
}

function checkGameOver(){
    let count_white = 0, count_black = 0;

    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
                count_white++;
            }
            if(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER){
                count_black++;
            }
        }
    }
    if (count_white === 0) {
        WIN = BLACK_PLAYER
    }

    if(count_black === 0){
        WIN = WHITE_PLAYER
    }
    return count_white === 0 || count_black === 0;

}

function view_checkers_on_obligatory_moves(td){
    let type = getTypeCheckerByTd(td);
    let white = false;
    let black = false;
    if(type === WHITE_CHECKER || type === WHITE_CROWN_CHECKER ){
        white = true;
    }else if(type === BLACK_CHECKER || type === BLACK_CROWN_CHECKER ){
        black = true;
    }
    if(white && player_move === WHITE_PLAYER ||
        black && player_move === BLACK_PLAYER) {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if(white && (start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER)){
                    let td = getTdByCoordinate(i, j);
                    handler_moves(td);
                    if (obligatory_moves.length !== 0) {
                        checkers_on_obligatory_moves.push(td);
                    }
                    reset_color_moves();
                }

                if(black && (start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER)){
                    let td = getTdByCoordinate(i, j);
                    handler_moves(td);
                    if (obligatory_moves.length !== 0) {
                        checkers_on_obligatory_moves.push(td);
                    }
                    reset_color_moves();
                }

            }
        }
    }
}

function checkPlayerMove(td){
    if(!td.hasChildNodes()){
        return false;
    }
    return td.firstChild.className === player_move;
}

function setActiveButtonsInfoMove(active){
    let button1 = document.querySelector(".complete-move-btn");
    let button2 = document.querySelector(".undo-move-btn");
    if(active){
        button1.style.backgroundColor = "#007765";
        button2.style.backgroundColor = "#007765";
    }else{
        button1.style.backgroundColor = "darkgrey";
        button2.style.backgroundColor = "darkgrey";
    }
}

let allTextWhite = [];
let allTextBlack = [];
let numbers_moves = [];
let SYMBOL;
function make_recording_moves(symbol2){
    let symbol;
    if(!symbol2){
        symbol = '-';
        if(knocked_checker_by_obligatory_moves.size !== 0) {
            symbol = ':';
        }
    }else{
        symbol = SYMBOL;
    }

    let move;
    let num = document.querySelector(".numbers-moves");
    let recordMoves = document.querySelector(".record-moves");
    if(player_move === BLACK_PLAYER){


        let p1 = document.createElement("p");
        p1.setAttribute("class", "leftstr");
        p1.innerText = last_move_from.id.toString() + symbol + last_move_to.id.toString() + " ";
        recordMoves.appendChild(p1);
        // count_moves++;
        // numbers_moves.push(count_moves + "\n");
        // let txt = "";
        // for(let num1 of numbers_moves){
        //     txt = txt + num1;
        // }
        // num.textContent = txt;
        // txt = "";
        // move = document.querySelector(".text-moves-white");
        // allTextWhite.push(last_move_from.id.toString() + symbol + last_move_to.id.toString() + '\n');
        // for(let s of allTextWhite ){
        //     txt = txt + s;
        // }
        // move.textContent = txt;
    }else{
        let p1 = document.createElement("p");
        p1.setAttribute("class", "rightstr");
        p1.innerText = last_move_from.id.toString() + symbol + last_move_to.id.toString() + "\n";
        recordMoves.appendChild(p1);
        // let txt = "";
        // move = document.querySelector(".text-moves-black");
        // allTextBlack.push(last_move_from.id.toString() + symbol + last_move_to.id.toString() +'\n');
        // for(let s of allTextBlack){
        //     txt = txt + s;
        // }
        // move.textContent = txt;
    }
}

function remove_recording_moves(){
    // let numbers = document.querySelector(".numbers-moves");
    // numbers.textContent = "";
    // let whites = document.querySelector(".text-moves-white");
    // whites.textContent = "";
    // let blacks = document.querySelector(".text-moves-black");
    // blacks.textContent = "";
    // allTextWhite = [];
    // allTextBlack = [];
    // numbers_moves = [];
    // count_moves = 0;
    let rcrd = document.querySelector(".record-moves");
    rcrd.innerHTML = '';
    // let childs = rcrd.childNodes;
    // for (let i = 0; i < childs.length; i++) {
    //     if(rcrd.lastChild.nodeName === "P"){
    //         rcrd.lastChild.remove();
    //     }
    // }
}

let complete_btn = document.querySelector(".complete-move-btn");
complete_btn.addEventListener("click", function (){
    if(state_buttons_info){
        state_buttons_info = false;
        setActiveButtonsInfoMove(state_buttons_info);
        if(player_move === WHITE_PLAYER){
            player_move = BLACK_PLAYER;
        }else{
            player_move = WHITE_PLAYER;
        }
        let i = getCoordinateIByTd(last_move_to);
        let type = getTypeCheckerByTd(last_move_to);
        if(i === 0 && type === BLACK_CHECKER){
            checkerToCrown(last_move_to, BLACK_CROWN_CHECKER);
        }
        if(i === 7 && type === WHITE_CHECKER){
            checkerToCrown(last_move_to, WHITE_CROWN_CHECKER);
        }
        
        if(knocked_checker_by_obligatory_moves.size !== 0){
            knocked_checker(last_move_to);
        }
        print_table(start_table);
        changeInfoPlayerMove(player_move);
        make_recording_moves(false);
        knocked_checker_by_obligatory_moves.clear();
        if(checkGameOver()){
            if(WIN === BLACK_PLAYER){
                console.log("Победил Черный!")
            }else{
                console.log("Победил Белый!")
            }
            show_who_win();
        }
    }
});

function show_who_win(){
    let popup = document.querySelector(".popup-info");
    popup.style.display = 'block';
    popup.style.position = 'absolute';
    let text;
    if(WIN === BLACK_PLAYER){
        text = "ПОБЕДИЛ ЧЕРНЫЙ!";
    }else{
        text = "ПОБЕДИЛ БЕЛЫЙ!";
    }
    let p = document.createElement("p");
    p.textContent = text;
    popup.appendChild(p);
}

let undo_btn = document.querySelector(".undo-move-btn");
undo_btn.addEventListener("click", function (){
    if(state_buttons_info){
        undo_last_move();
        state_buttons_info = false;
        setActiveButtonsInfoMove(state_buttons_info);
        knocked_checker_by_obligatory_moves.clear();
         // reset_color_moves();
    }
});

function knocked_checker(checker){ //срубить шашку
    let td = knocked_checker_by_obligatory_moves.get(checker);
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    start_table[i][j] = 0;
    print_table(start_table);
}

let read_btn = document.querySelector(".read-record-button");
read_btn.addEventListener("click", function (){
    readTextArea();
});

function getTdById(id){
    return document.getElementById(id);
}

function checkExistIdInAllTable(id){
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            let td = getTdById(id);
            if(td !== null && td.id === getTdByCoordinate(i, j).id){
                return true;
            }
        }
    }
    return false;
}

function checkExistIdInCurrentTable(id){
    let num = getIndexByNumber(Number(id.substring(1)))
    let char = getIndexByChar(id.substring(0, 1));
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if(start_table[i][j] !== EMPTY_CELL){
                if(i === num && char === j){
                    return true;
                }
            }
        }
    }
    return false;
}

function isWhitePlayerMove(id){
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            let td = getTdByCoordinate(i, j);
            if(td.id === id){
                return start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER;
            }
        }
    }
}
function isCrownChecker(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    return start_table[i][j] === WHITE_CROWN_CHECKER ||
        start_table[i][j] === BLACK_CROWN_CHECKER;
}

let ERROR_MESSAGE;
let error = false;
function check_read_moves(arr, index, player){
    // if(arr[index] === ""){
    //     ERROR_MESSAGE = "Пустая строка в записи!";
    //     return false;
    // }

    if(arr[index].length !== 5){
        ERROR_MESSAGE = "Некорректная запись - " + arr[index];
        return false;
    }
    
    let idFrom = arr[index].substring(0, 2);
    let idTo = arr[index].substring(3);
    let sign = arr[index].charAt(2);


    if(!checkExistIdInAllTable(idFrom) || !checkExistIdInAllTable(idTo) || sign!== ":" && sign !== "-"){
        ERROR_MESSAGE = "Невозможный ход - " + arr[index];
        return false;
    }
    if(!checkExistIdInCurrentTable(idFrom)){
        ERROR_MESSAGE = "Невозможный ход  - " + arr[index];
        return false;
    }
    let td = getTdById(idFrom);

    if(player === BLACK_PLAYER){
        //Если шашка- дамка, то просвеяичваем ходы дял дамки
        if(isCrownChecker(td)){
            show_moves_crown(td, false);
        }else{
            show_moves_black(td, false);
        }
        changeInfoPlayerMove(WHITE_PLAYER);
    }

    if(player === WHITE_PLAYER){
        //Если шашка - дамка, то просвеяичваем ходы дял дамки
        if(isCrownChecker(td)){
            show_moves_crown(td, false);
        }else show_moves_white(td, false);
        changeInfoPlayerMove(BLACK_PLAYER);
    }

    if(sign === "-" && !possible_moves.includes(getTdById(idTo))){
        ERROR_MESSAGE = "Невозможный ход - " + arr[index];
        return false;
    }

    if(sign === ":" && !obligatory_moves.includes(getTdById(idTo))){
        ERROR_MESSAGE = "Невозможный ход - " + arr[index];
        return false;
    }

    let i = getCoordinateIByTd(getTdById(idFrom));
    let j = getCoordinateJByTd(getTdById(idFrom));
    let k = getCoordinateIByTd(getTdById(idTo));
    let l = getCoordinateJByTd(getTdById(idTo));

    if(sign === ":"){
        //Делаем пустой клетку между шашками
        let id = getIdBetweenFromAndTo(idFrom, idTo);
        let index_num_id = getIndexByNumber(Number(id.slice(1)));
        let index_char_id = getIndexByChar(id.slice(0, 1));
        start_table[index_num_id][index_char_id] = 0;
    }

    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    //проверка на дамку
    if(player === BLACK_PLAYER && k === 0){
        start_table[k][l] = BLACK_CROWN_CHECKER;
    }else if(player === WHITE_PLAYER && k === 7){
        start_table[k][l] = WHITE_CROWN_CHECKER;
    }else start_table[k][l] = temp;
    last_move_from = getTdById(idFrom);
    last_move_to = getTdById(idTo);
    SYMBOL = sign;
    make_recording_moves(true);
    if(checkGameOver()){
        show_who_win();
    }
    print_table(start_table);
    return true;
}

function copyArr(arr){
    let to = [];
    for (let i = 0; i < arr.length; i++) {
        to[i] = [];
        for (let j = 0; j < arr.length; j++) {
            to[i][j] = arr[i][j];
        }
    }
    return to;
}

function readTextArea(){
    // let area1 = document.querySelector(".record-moves-white-checkers");
    // let area2 = document.querySelector(".record-moves-black-checkers");
    // let text_black_moves = area2.value;
    // let text_white_moves = area1.value;
    let area = document.querySelector(".read-record-textarea");
    let text = area.value;
    let arrayString = text.toString().split("\n");
    let arrOfWhiteMoves = [];
    let arrOfBlackMoves = [];
    for(let str of arrayString){
        arrOfWhiteMoves.push(str.substring(0, 5));
        if(str.length > 5){
            arrOfBlackMoves.push(str.substring(6));
        }
    }
    //Делаем копию исходного массива до чтения ходов, чтобы в случае ошибки сделать откат
    let original_state = copyArr(start_table);
    let original_player_move = player_move;

    if(text !== ""){
        // let arrOfWhiteMoves = text_white_moves.toString().split(" ");
        // let arrOfBlackMoves = text_black_moves.toString().split(" ");

        if(arrOfWhiteMoves.length - arrOfBlackMoves.length < 0 ||
            arrOfWhiteMoves.length - arrOfBlackMoves.length > 1){
            error = true;
            ERROR_MESSAGE = "Ошибка в общей структуре!";
        }else {
            //индекс гоним до размера белых ходов, так как невозможна ситуация когда черных будет больше
            for (let index = 0; index < arrOfWhiteMoves.length; index++) {
                if (!check_read_moves(arrOfWhiteMoves, index, WHITE_PLAYER)) {
                    error = true;
                    start_table = copyArr(original_state);
                    player_move = original_player_move;
                    remove_recording_moves();
                    print_table(start_table);
                    // console.log(ERROR_MESSAGE);
                    break;
                }
                if (index < arrOfBlackMoves.length && !check_read_moves(arrOfBlackMoves, index, BLACK_PLAYER)) {
                    error = true;
                    start_table = copyArr(original_state);
                    player_move = original_player_move;
                    remove_recording_moves();
                    print_table(start_table);
                    // console.log(ERROR_MESSAGE);
                    break;
                }

                possible_moves = [];
                obligatory_moves = [];
            }
        }

    }else{
        error = true;
        ERROR_MESSAGE = "Пустое поле!";
    }
    changeInfoPlayerMove(player_move);
    if(error){
        let popup = document.querySelector(".popup-info");
        popup.style.display = 'block';
        popup.style.position = 'absolute';
        let text;
        let p = document.createElement("p");
        p.textContent = ERROR_MESSAGE;
        popup.appendChild(p);
        error = false;
    }
    knocked_checker_by_obligatory_moves.clear();
}

function getIdBetweenFromAndTo(from, to){
    let result_index_num, result_index_char;
    let char_from = from.slice(0, 1);
    let num_from = from.slice(1);
    let char_to = to.slice(0, 1);
    let num_to = to.slice(1);
    let index_char_from = getIndexByChar(char_from);
    let index_num_from = getIndexByNumber(Number(num_from));
    let index_char_to = getIndexByChar(char_to);
    let index_num_to = getIndexByNumber(Number(num_to));

    if(index_num_from > index_num_to){
        result_index_num = index_num_to + 1;
    }else{
        result_index_num = index_num_to - 1;
    }
    if(index_char_from > index_char_to){
        result_index_char = index_char_to + 1;
    }else{
        result_index_char = index_char_to - 1;
    }
    return alphabet[result_index_char] + numbers[result_index_num];
}

