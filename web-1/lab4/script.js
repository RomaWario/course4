let start_table = [];
let alphabet1 = ["a", "b", "c", "d", "e", "f", "g", "h"];
let alphabet = alphabet1.reverse();
let numbers1 = [8, 7, 6, 5, 4, 3, 2, 1];
let numbers = numbers1.reverse();
let selectedTd = 0;
let flag = false;
let possible_moves = [];
let obligatory_moves = [];
const
    BLACK_CHECKER = 2,
    WHITE_CHECKER = 1,
    BLACK_CROWN_CHECKER = 4,
    WHITE_CROWN_CHECKER = 3,
    EMPTY_CELL = 0;

function start_init_table() {
    let type_checker = WHITE_CHECKER;
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(i > 2){
                type_checker = BLACK_CHECKER;
            }
            if (i === 3 || i === 4) {
                start_table[i][j] = EMPTY_CELL;
            } else {
                if (i % 2 === 0) {
                    if (j % 2 === 0) {
                        start_table[i][j] = EMPTY_CELL;
                    } else {
                        start_table[i][j] = type_checker;
                    }
                } else {
                    if (j % 2 === 0) {
                        start_table[i][j] = type_checker;
                    } else {
                        start_table[i][j] = EMPTY_CELL;
                    }
                }
            }
        }
    }
}

function getIndexByChar(char){
    return alphabet.indexOf(char);
}

function getIndexByNumber(num){
    return numbers.indexOf(num);
}

function example_init_table(){
    for (let i = 0; i < 8; i++) {
        start_table[i] = [];
        for (let j = 0; j < 8; j++) {
            if(j === getIndexByChar('f') && i === getIndexByNumber(4) ||
                j === getIndexByChar('h') && i === getIndexByNumber(4)){
                start_table[i][j] = WHITE_CHECKER;
            }else if(
                j === getIndexByChar('b') && i === getIndexByNumber(8) ||
                j === getIndexByChar('c') && i === getIndexByNumber(5) ||
                j === getIndexByChar('c') && i === getIndexByNumber(7) ||
                j === getIndexByChar('e') && i === getIndexByNumber(7) ||
                j === getIndexByChar('h') && i === getIndexByNumber(6)){
                start_table[i][j] = BLACK_CHECKER;
            }else if(j === getIndexByChar('c') && i === getIndexByNumber(1)) {
                start_table[i][j] = BLACK_CROWN_CHECKER;
            }else {
                start_table[i][j] = EMPTY_CELL;
            }
        }
    }
}

function clear(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].hasChildNodes()){
            let img = blacks[i].firstChild.remove();
        }
        if(blacks[i].style.backgroundColor === 'yellow'){
            blacks[i].style.backgroundColor = '#a9a9a9';
        }
    }
    reset_color_moves();
    // reset_color_moves("green");
    // reset_color_moves("red");
}

function print_table(table){
    clear();
    for (let i = 0; i < 8; i++) {
        let number = numbers[i];
        for (let j = 0; j < 8; j++) {
            let elem = document.getElementById(alphabet[j] + number);
            if(table[i][j] === WHITE_CHECKER){
                let img = document.createElement("img");
                img.setAttribute("src", "img/white.png");
                img.setAttribute("height", "31px");
                img.setAttribute("width", "30px");
                img.setAttribute("alt", "white");
                elem.appendChild(img);
                img.setAttribute("id", "img")
            }

            if(table[i][j] === BLACK_CHECKER){
                let img = document.createElement("img")
                img.setAttribute("src", "img/black.png");
                img.setAttribute("height", "31px");
                img.setAttribute("width", "30px");
                img.setAttribute("alt", "black");
                elem.appendChild(img);
            }

            if(table[i][j] === WHITE_CROWN_CHECKER){
                let img = document.createElement("img")
                img.setAttribute("src", "img/white_crown.png");
                img.setAttribute("height", "31px");
                img.setAttribute("width", "30px");
                img.setAttribute("alt", "white-crown");
                elem.appendChild(img);
            }

            if(table[i][j] === BLACK_CROWN_CHECKER){
                let img = document.createElement("img")
                img.setAttribute("src", "img/black_crown.png");
                img.setAttribute("height", "31px");
                img.setAttribute("width", "30px");
                img.setAttribute("alt", "black-crown");
                elem.appendChild(img);
            }
        }
    }
}

let start_btn = document.querySelector(".start-button");
start_btn.addEventListener("click", function (){
    start_init_table();
    print_table(start_table);
});

let exp_btn = document.querySelector(".example-button");
exp_btn.addEventListener("click", function (){
    example_init_table();
    print_table(start_table);
});

let cur_state = document.querySelector(".current-state-table-button");
cur_state.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    popup.style.display = 'block';
    popup.style.position = 'absolute';
    let text = current_state_table();
    let wh = text.slice(0, text.indexOf("Черные"));
    let bl = text.slice(text.indexOf("Черные"));
    let p = document.createElement("p");
    p.className = "text-black";
    p.textContent = wh;
    let p2 = document.createElement("p");
    p2.className = "text-white";
    p2.textContent = bl;
    popup.appendChild(p);
    popup.appendChild(p2);
});

let close_btn = document.querySelector(".popup-close-button");

close_btn.addEventListener("click", function (){
    let popup = document.querySelector(".popup-info");
    let p1 = document.querySelector(".text-black");
    let p2 = document.querySelector(".text-white");
    p1.remove();
    p2.remove();
    popup.style.display = 'none';
});

let table = document.querySelector("table");

function checkActive(td){
    if(td.id === selectedTd.id){
        return true;
    }
    return selectedTd.style.backgroundColor !== 'yellow';
}

function checkerToCrown(td, typeChecker){
    if(typeChecker !== WHITE_CROWN_CHECKER && typeChecker !== BLACK_CROWN_CHECKER){
        return;
    }
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    start_table[i][j] = typeChecker;
}

function show_moves_black(td){
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 0){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num - 1;
    let j = index_char + 1;
    //Ход верх-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
    }
    if(!h && start_table[i][j] === WHITE_CHECKER){
        let k = i - 1;
        let l = j + 1;
        if(l <= 7 && k >= 0){
            remove_green_moves();
            if(start_table[k][l] === EMPTY_CELL){
                let td = document.getElementById(alphabet[l] + numbers[k]);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
    //Ход верх-лево
    i = index_num - 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
        return;
    }
    if(!a && start_table[i][j] === WHITE_CHECKER) {
        let k = i - 1;
        let l = j - 1;
        if (l >= 0 && k >= 0) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_white(td){
    let index_char = alphabet.indexOf(td.id.toString().slice(0, 1));
    let index_num = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(index_num === 7){
        return ;
    }
    let a = index_char === 0;
    let h = index_char === 7;
    let i = index_num + 1;
    let j = index_char + 1;
    //Ход низ-право
    if(!h && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
    }
    if(!h && start_table[i][j] === BLACK_CHECKER){
        let k = i + 1;
        let l = j + 1;
        if(l <= 7 && k <= 7){
            if(start_table[k][l] === EMPTY_CELL){
                let td = document.getElementById(alphabet[l] + numbers[k]);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
    //Ход низ-лево
    i = index_num + 1;
    j = index_char - 1;
    if(!a && start_table[i][j] === EMPTY_CELL){
        let td = document.getElementById(alphabet[j] + numbers[i]);
        possible_moves.push(td);
        td.style.border = '3px solid green';
        return;
    }
    if(!a && start_table[i][j] === BLACK_CHECKER) {
        let k = i + 1;
        let l = j - 1;
        if (l >= 0 && k <= 7) {
            if (start_table[k][l] === EMPTY_CELL) {
                let td = document.getElementById(alphabet[l] + numbers[k]);
                td.style.border = '3px solid red';
                obligatory_moves.push(td);
            }
        }
    }
}

function show_moves_crown(td){
    let type  = getTypeCheckerByTd(td);
    let coordI = getCoordinateIByTd(td);
    let coordJ = getCoordinateJByTd(td);
    let blockBR = false, blockBL = false, blockTR = false, blockTL = false;
    let bottomRight = 1, bottomLeft = 1, topRight = 1, topLeft = 1;
    for (let i = coordI; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI + bottomRight && j === coordJ + bottomRight){
                //BR
                if(!blockBR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }

                if(!blockBR && i < 7 && j < 7 && start_table[i][j] === WHITE_CHECKER){
                    let k = i + 1, l = j + 1;
                    console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }

                if(!blockBR && i < 7 && j < 7 && start_table[i][j] === BLACK_CHECKER){
                    let k = i + 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBR = true;
                }
                bottomRight++;
            }
            if(i === coordI + bottomLeft && j === coordJ - bottomLeft){
                //BL
                if(!blockBL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }
                if(!blockBL && i < 7 && j > 0 && start_table[i][j] === WHITE_CHECKER){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }

                if(!blockBL && i < 7 && j > 0 && start_table[i][j] === BLACK_CHECKER){
                    let k = i + 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockBL = true;
                }
                bottomLeft++;
            }
        }
    }
    for (let i = coordI; i >= 0; i--) {
        for (let j = 0; j < 8; j++) {
            if(i === coordI - topRight && j === coordJ + topRight){
                //TR
                if(!blockTR && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }

                if(!blockTR && i > 0 && j < 7 && start_table[i][j] === WHITE_CHECKER){
                    let k = i - 1, l = j + 1;
                    console.log(getTdByCoordinate(k, l));
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }

                if(!blockTR && i > 0 && j < 7 && start_table[i][j] === BLACK_CHECKER){
                    let k = i - 1, l = j + 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTR = true;
                }
                topRight++;
            }
            if(i === coordI - topLeft && j === coordJ - topLeft){
                //TL
                if(!blockTL && start_table[i][j] === EMPTY_CELL){
                    let td = getTdByCoordinate(i, j);
                    td.style.border = '3px solid green';
                    possible_moves.push(td);
                }
                if(!blockTL && i > 0 && j > 0 && start_table[i][j] === WHITE_CHECKER){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === BLACK_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }

                if(!blockTL && i > 0 && j > 0 && start_table[i][j] === BLACK_CHECKER){
                    let k = i - 1, l = j - 1;
                    if(start_table[k][l] === EMPTY_CELL && type === WHITE_CROWN_CHECKER){
                        let td = getTdByCoordinate(k, l);
                        td.style.border = '3px solid red';
                        obligatory_moves.push(td);
                    }
                    blockTL = true;
                }
                topLeft++;
            }
        }
    }
}

function reset_color_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'||
            blacks[i].style.border === '3px solid red'){
            blacks[i].style.border = '1px solid black';
        }
    }
    possible_moves = [];
    obligatory_moves = [];
}

function remove_green_moves(){
    let blacks = document.getElementsByClassName("black");
    for (let i = 0; i < blacks.length; i++) {
        if(blacks[i].style.border === '3px solid green'){
            blacks[i].style.border = '1px solid black';
        }
    }
     possible_moves = [];
}

function make_move(from, to){
    let i = getCoordinateIByTd(from);
    let j = getCoordinateJByTd(from);
    let k = getCoordinateIByTd(to);
    let l = getCoordinateJByTd(to);
    let type = getTypeCheckerByTd(from);
    let temp = start_table[i][j];
    start_table[i][j] = start_table[k][l];
    start_table[k][l] = temp;
    if(k === 0 && type === BLACK_CHECKER){
        checkerToCrown(to, BLACK_CROWN_CHECKER);
    }
    if(k === 7 && type === WHITE_CHECKER){
        checkerToCrown(to, WHITE_CROWN_CHECKER);
    }
    print_table(start_table);
}

function getTypeCheckerByTd(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    return start_table[i][j];
}

function getTdByCoordinate(i, j){
   return document.getElementById(alphabet[j] + numbers[i]);
}

function getCoordinateIByTd(td){
    return numbers.indexOf(Number(td.id.toString().slice(1)));;
}

function getCoordinateJByTd(td){
    return alphabet.indexOf(td.id.toString().slice(0, 1));
}

function highlight_cell(td){
    if(td.style.backgroundColor === 'yellow'){
        reset_color_moves();
        selectedTd.style.backgroundColor = '#a9a9a9';
    }else {
        selectedTd = td;
        selectedTd.style.backgroundColor = 'yellow';
        flag = true;
    }
}

function current_state_table(){
    let white = 'Белые: ';
    let black = 'Черные: ';
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            let td = getTdByCoordinate(i, j);
            if(start_table[i][j] === WHITE_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
                white += td.id.toString() + ' ';
            }
            if(start_table[i][j] === BLACK_CHECKER || start_table[i][j] === BLACK_CROWN_CHECKER){
                black += td.id.toString() + ' ';
            }
        }
    }
    return white + black;
}

start_init_table();
print_table(start_table);

function handler_moves(td){
    let i = getCoordinateIByTd(td);
    let j = getCoordinateJByTd(td);
    if(start_table[i][j] === BLACK_CROWN_CHECKER || start_table[i][j] === WHITE_CROWN_CHECKER){
        show_moves_crown(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === BLACK_CHECKER){
        show_moves_black(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
    if(start_table[i][j] === WHITE_CHECKER){
        show_moves_white(td);
        if(obligatory_moves.length !== 0){
            remove_green_moves();
        }
    }
}

table.onclick = function (event){
    let td = event.target.closest('td'); // (1)
  //  if (td.className !== "black") return; // (2)
    if (!table.contains(td)) return; // (3)

    if(obligatory_moves.length === 0){
        for (let i = 0; i < possible_moves.length; i++) {
            if(td === possible_moves[i]){
                make_move(selectedTd, possible_moves[i]);
                return ;
            }
        }
    }else {
        for (let i = 0; i < obligatory_moves.length; i++) {
            if (td === obligatory_moves[i]) {
                make_move(selectedTd, obligatory_moves[i]);
                return;
            }
        }
    }

    let j = alphabet.indexOf(td.id.toString().slice(0, 1));
    let i = numbers.indexOf(Number(td.id.toString().slice(1)));
    if(start_table[i][j] === EMPTY_CELL){
        return;
    }

    if(flag && !checkActive(td))  return;
    handler_moves(td);
    highlight_cell(td);
}
