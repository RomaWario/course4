package urlconnection;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class URLReader {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите URL: ");
        String str = in.nextLine();
        URL url = new URL(str);
        URLConnection urlConnection = url.openConnection();
        urlConnection.connect();
        Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
        for(String string : headerFields.keySet()){
            System.out.println(string );
            System.out.println("Fields: ");
            for(String string1: headerFields.get(string)){
                System.out.println(string1);
            }
            System.out.println();
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String inputLine;
        String fileName = "index.html";
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        while ((inputLine = bufferedReader.readLine()) != null) {
            bufferedWriter.write(inputLine);
        }

        bufferedReader.close();
        bufferedWriter.close();
    }
}
