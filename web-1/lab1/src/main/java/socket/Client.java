package socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public Client(){}

    public static void main(String[] args) throws Exception{
        Scanner in = new Scanner(System.in);
        System.out.println("Введите URL: ");
        String url = in.nextLine();
        try {
            Socket socket = new Socket(url, 80);
            PrintStream printStream = new PrintStream(socket.getOutputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printStream.println("GET " + "/index.html" + " HTTP/1.0");
            printStream.println();
            String inputLine;
            File file = new File("index.html");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            String line = bufferedReader.readLine();
            boolean flag = true;
            while (line != null) {
                if(line.startsWith("<")){
                   flag = false;
                }
                if(flag){
                    System.out.println(line);
                }else{
                    bufferedWriter.write(line);
                }
                line = bufferedReader.readLine();
            }
            bufferedWriter.close();
            fileWriter.close();
            printStream.close();
            bufferedReader.close();
            socket.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
