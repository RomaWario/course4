package ru.omsu.imit.toast;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showToast(View view) {
//        Toast toast = Toast.makeText(getApplicationContext(),
//                "Пора покормить кота!",
//                Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//        Toast toast3 = Toast.makeText(getApplicationContext(),
//                R.string.catfood, Toast.LENGTH_LONG);
//        toast3.setGravity(Gravity.CENTER, 0, 0);
//        LinearLayout toastContainer = (LinearLayout) toast3.getView();
//        ImageView catImageView = new ImageView(getApplicationContext());
//        catImageView.setImageResource(R.drawable.cat);
//        assert toastContainer != null;
//        toastContainer.addView(catImageView, 0);
//        toast3.show();
//        int duration = Toast.LENGTH_LONG;
//        Toast toast = Toast.makeText(getApplicationContext(),
//                R.string.catfood,
//                duration);
//        toast.setGravity(Gravity.TOP, 0, 0);
//        toast.show();
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}