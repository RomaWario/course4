package ru.omsu.imit.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    Context mContext;
    private final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        String action = intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION)) {
            // здесь ваш код
            // например, запускаем уведомление
//            Intent intent = new Intent(context, ru.alexanderklimov.NotifyService.NotifyService.class);
//            context.startService(intent);
//            // в общем виде
//            //для Activity
//            Intent activivtyIntent = new Intent(context, MyActivity.class);
//            activivtyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(activivtyIntent);
//
//            //для Service
//            Intent serviceIntent = new Intent(context, MyService.class);
//            context.startService(serviceIntent);
        }
    }
}