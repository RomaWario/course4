package ru.omsu.imit.counter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView helloTextView = findViewById(R.id.textView);;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      //  mCrowsCounterButton = findViewById(R.id.button2);
    }

    public void onClick(View view){
        helloTextView.setText("Hello Kitty!");
    }

    public void onClickCounterButton(View view){
        helloTextView.setText("Я насчитал " + ++counter + " ворон");
    }
}