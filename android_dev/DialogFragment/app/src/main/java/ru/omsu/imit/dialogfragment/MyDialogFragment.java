package ru.omsu.imit.dialogfragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class MyDialogFragment extends DialogFragment {

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Важное сообщение!")
//                .setMessage("Покормите кота!")
//                .setIcon(R.drawable.ic_launcher_cat)
//                .setPositiveButton("ОК, иду на кухню", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // Закрываем окно
//                        dialog.cancel();
//                    }
//                });
//        return builder.create();
//    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        String title = "Выбор есть всегда";
//        String message = "Выбери пищу";
//        String button1String = "Вкусная пища";
//        String button2String = "Здоровая пища";
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(title);  // заголовок
//        builder.setMessage(message); // сообщение
//        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                Toast.makeText(getActivity(), "Вы сделали правильный выбор",
//                        Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                Toast.makeText(getActivity(), "Возможно вы правы", Toast.LENGTH_LONG)
//                        .show();
//            }
//        });
//        builder.setCancelable(true);
//
//        return builder.create();
//    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setMessage("Выберите правильный ответ")
//                .setCancelable(true)
//                .setPositiveButton("Мяу",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                dialog.cancel();
//                            }
//                        })
//                .setNeutralButton("Гав",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                dialog.cancel();
//                            }
//                        })
//                .setNegativeButton("Сам дурак!",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                dialog.cancel();
//                            }
//                        });
//
//        return builder.create();
//    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        final String[] catNamesArray = {"Васька", "Рыжик", "Мурзик"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Выберите кота")
//                .setItems(catNamesArray, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(getActivity(),
//                                "Выбранный кот: " + catNamesArray[which],
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//        return builder.create();
//    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        final String[] catNamesArray = {"Васька", "Рыжик", "Мурзик"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Выберите любимое имя кота")
//                // добавляем переключатели
//                .setSingleChoiceItems(catNamesArray, -1,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int item) {
//                                Toast.makeText(
//                                        getActivity(),
//                                        "Любимое имя кота: "
//                                                + catNamesArray[item],
//                                        Toast.LENGTH_SHORT).show();
//                            }
//                        })
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // User clicked OK, so save the mSelectedItems results somewhere
//                        // or return them to the component that opened the dialog
//
//                    }
//                })
//                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//
//                    }
//                });
//
//        return builder.create();
//    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        final String[] catNamesArray = {"Васька", "Рыжик", "Мурзик"};
//        final boolean[] checkedItemsArray = {false, true, false};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Выберите котов")
//                .setMultiChoiceItems(catNamesArray, checkedItemsArray,
//                        new DialogInterface.OnMultiChoiceClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which, boolean isChecked) {
//                                checkedItemsArray[which] = isChecked;
//                            }
//                        })
//                .setPositiveButton("Готово",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                StringBuilder state = new StringBuilder();
//                                for (int i = 0; i < catNamesArray.length; i++) {
//                                    state.append(catNamesArray[i]);
//                                    if (checkedItemsArray[i])
//                                        state.append(" выбран\n");
//                                    else
//                                        state.append(" не выбран\n");
//                                }
//                                Toast.makeText(getActivity(),
//                                        state.toString(), Toast.LENGTH_LONG)
//                                        .show();
//                            }
//                        })
//
//                .setNegativeButton("Отмена",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
//                                dialog.cancel();
//                            }
//                        });
//
//        return builder.create();
//    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Вы жертвуете миллион коту")
                .setIcon(R.drawable.ic_launcher_cat)
                .setTitle("Важно! Максимальный перепост")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getActivity()).okClicked();
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getActivity()).cancelClicked();
                    }
                });

        return builder.create();
    }
}