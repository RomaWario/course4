package ru.omsu.imit.dialogfragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        FragmentManager manager = getSupportFragmentManager();
        MyDialogFragment myDialogFragment = new MyDialogFragment();
        myDialogFragment.show(manager, "myDialog");
    }

    public void okClicked() {
        Toast.makeText(getApplicationContext(), "Вы выбрали кнопку OK!",
                Toast.LENGTH_LONG).show();
    }

    public void cancelClicked() {
        Toast.makeText(getApplicationContext(), "Вы выбрали кнопку отмены!",
                Toast.LENGTH_LONG).show();
    }

//    public void onClick(View v) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
//        builder.setTitle("Автоматическое закрытие окна");
//        builder.setMessage("Через пять секунд это окно закроется автоматически!");
//        builder.setCancelable(true);
//
//        final AlertDialog dlg = builder.create();
//
//        dlg.show();
//
//        final Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            public void run() {
//                dlg.dismiss(); // when the task active then close the dialog
//                timer.cancel(); // also just top the timer thread, otherwise,
//                // you may receive a crash report
//            }
//        }, 5000); // через 5 секунд (5000 миллисекунд), the task will be active.
//    }

//    public void onClick(View view) {
//        MyDialogFragment myDialogFragment = new MyDialogFragment();
//        FragmentManager manager = getSupportFragmentManager();
//        //myDialogFragment.show(manager, "dialog");
//
//        FragmentTransaction transaction = manager.beginTransaction();
//        myDialogFragment.show(transaction, "dialog");
//    }
}