package ru.omsu.imit.preferencesframework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Кнопка для запуска экрана настроек
    public void onClick(View v){
        showSettings();
    }

    public void showSettings() {
        Intent intent = new Intent(MainActivity.this, MyPreferenceActivity.class);
        startActivityForResult(intent, 0);
    }

    // получаем результат
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);

        int catHeight = sharedPreferences.getInt("height", 20);
        // Добавим TextView, в котором будем выводить значение настройки
        settingCheckBox.setText("Высота кота = "
                + catHeight);
    }
}