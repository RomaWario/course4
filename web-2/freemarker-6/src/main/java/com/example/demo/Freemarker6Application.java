package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Freemarker6Application {

	public static void main(String[] args) {
		SpringApplication.run(Freemarker6Application.class, args);
	}

}
