package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Groovy7Application {

	public static void main(String[] args) {
		SpringApplication.run(Groovy7Application.class, args);
	}

}
