package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Thymeleaf5Application {

	public static void main(String[] args) {
		SpringApplication.run(Thymeleaf5Application.class, args);
	}

}
