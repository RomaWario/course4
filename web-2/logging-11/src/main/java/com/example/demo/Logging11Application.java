package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Logging11Application {

	public static void main(String[] args) {
		SpringApplication.run(Logging11Application.class, args);
	}

}
