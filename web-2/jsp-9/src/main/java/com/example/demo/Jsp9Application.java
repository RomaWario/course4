package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jsp9Application {

	public static void main(String[] args) {
		SpringApplication.run(Jsp9Application.class, args);
	}

}
