package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Beginners3Application {

	public static void main(String[] args) {
		SpringApplication.run(Beginners3Application.class, args);
	}

}
